describe('test app', function () {
    it('Access the application', function () {
        cy.visit('http://localhost:3000/');
    });

    it('Search accepts input', function () {
        const text = 'Guacamole';
        cy
            .get('input')
            .type(text)
            .should('have.value', text);
    });

    it('Check the first result', function () {
        cy.get(':nth-child(1) > .LazyLoad').click();
        cy.contains('Forks:');
    });

    it('Go back to the search results', function () {
        cy.contains('Back').click();
    });

    it('Change pages', function () {
        cy.get('.next').click();
        cy.wait(2000);
        cy.get('.next').click();
        cy.wait(2000);
        cy.get('.previous').click();
        cy.wait(2000);
        cy.get('.pagination > :nth-child(10)').click();
    });

    it('Check a different result', function () {
        cy.wait(2000);
        cy.get(':nth-child(1) > .LazyLoad').click();
        cy.contains('Forks:');
        cy.contains('Back').click();
    });

    it('Search for a impossible query', function () {
        const text = 'abcd123456789+123456789';
        cy
            .get('input')
            .clear()
            .type(text)
            .should('have.value', text);
        cy.wait(2000);
        cy.contains('No repositories found');
    });

    it('Force an error search', function () {
        const text = ';;;;;;;;;;;;;;;';
        cy
            .get('input')
            .clear()
            .type(text)
            .should('have.value', text);
        cy.wait(2000);
        cy.contains('Error');
    });

});


