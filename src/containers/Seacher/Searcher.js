import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import * as actionTypes from '../../store/actions';

import Aux from '../../hoc/Auxiliary/Auxiliary';
// import Modal from '../../components/UI/Modal/Modal';
import UserInput from "../../components/UserInput/UserInput";
import QueryResults from '../../components/QueryResults/QueryResults';
import QueryMessage from '../../components/QueryResults/QueryMessage/QueryMessage';
import QueryDetails from '../../components/QueryResults/QueryDetails/QueryDetails';
import * as utils from '../../components/utils/utils';
import SearchTime from '../../components/UI/SearchTime/SearchTime';

class Searcher extends Component {

    constructor(props) {
        super(props);
        this.timeout = null;
    }

    /***
     * When the user stops typing, requests the query
     * @param event
     */
    changeSearchQueryHandler = (event) => {

        const query = event.target.value;
        this.props.onTyping(query);
        this.props.onShowMessage(false);
        this.props.onShowResults(false);
        this.props.onShowDetails(false);
        this.props.onShowSearchTime(false);

        if (query.length <= 0) {
            clearTimeout(this.timeout);

            this.props.onSearch([]);
            this.props.onErrorFound(null);
            return;
        }

        if (this.timeout) clearTimeout(this.timeout);

        let startCt = new Date().getTime();
        let searchTime;

        this.timeout = setTimeout(() => {
            axios.get(`https://api.github.com/search/repositories?q=${query}&page=1&per_page=${this.props.resultsPerPage}`)
                .then(response => {
                    let maxPages = Math.ceil(response.data.total_count / this.props.resultsPerPage);
                    this.props.onSearch(response.data.items, maxPages);
                    this.props.onShowResults(true);
                    this.props.onErrorFound(null);
                    this.props.onShowMessage(response.data.items.length === 0);

                    searchTime = utils.calculateSearchTime(startCt);
                    this.props.onSearchTime(searchTime);
                })
                .catch((error) => {
                    this.props.onSearch([]);
                    this.props.onShowResults(false);
                    this.props.onErrorFound(error.response.data);
                    this.props.onShowMessage(true);
                    this.props.onShowSearchTime(false);
                });
        }, 300);
    };

    /***
     * If this page was opened while having a repo ID, it opens its details
     */
    checkIfHaveIDHandler() {
        const hasId = window.location.hash.match('(#id)=([^&])+');

        if (hasId === null) return;

        if (hasId.length > 1) {
            const id = hasId[0].split('id=')[1];

            let startCt = new Date().getTime();
            let searchTime;

            axios.get('https://api.github.com/repositories/' + id)
                .then((response) => {
                    this.props.onQueryDetails(response.data, true);

                    searchTime = utils.calculateSearchTime(startCt);
                    this.props.onSearchTime(searchTime);
                    this.props.onErrorFound(null);
                })
                .catch((error) => {
                    this.props.onShowResults(false);
                    this.props.onErrorFound(error.response.data);
                    this.props.onShowMessage(true);
                    this.props.onShowSearchTime(false);
                });
        }
    }

    render() {
        // console.log('[Searcher.js] render');

        this.checkIfHaveIDHandler();

        return (
            <Aux>
                <UserInput type="text"
                           change={this.changeSearchQueryHandler}
                           txtPlaceHolder="Search for a repository!"/>
                <SearchTime/>
                <QueryMessage/>
                <QueryResults/>
                <QueryDetails/>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        resultsPerPage: state.resultsPerPage,
        maximumPages: state.maximumPages,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowResults: (value) => dispatch({type: actionTypes.SHOW_RESULTS, value}),
        onShowMessage: (value) => dispatch({type: actionTypes.SHOW_MESSAGE, value}),
        onErrorFound: (value) => dispatch({type: actionTypes.ERRORS_FOUND, value}),
        onSearch: (value, totalPages) => dispatch({
            type: actionTypes.SEARCHED,
            payload: {value, totalPages}
        }),
        onTyping: (value) => dispatch({type: actionTypes.TYPING, value}),
        onShowDetails: (value) => dispatch({type: actionTypes.SHOW_DETAILS, value}),
        onQueryDetails: (value, show) => dispatch({
            type: actionTypes.QUERY_DETAILS,
            payload: {value, show}
        }),
        onSearchTime: (value) => dispatch({type: actionTypes.SEARCH_TIME, value}),
        onShowSearchTime: (value) => dispatch({type: actionTypes.SHOW_SEARCH_TIME, value}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Searcher);