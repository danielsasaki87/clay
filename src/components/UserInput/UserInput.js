import React, {Component} from 'react';

import classes from './UserInput.css';
import {connect} from "react-redux";

class UserInput extends Component {

    render() {
        // console.log('[UserInput.js] render');

        return (
            <div className={classes.UserInput}>
                <input
                    type="text"
                    onChange={this.props.change}
                    placeholder={this.props.txtPlaceHolder}/>
                <div
                    className={classes.warning}
                    style={{
                        display: (
                            this.props.totalPages < this.props.maximumPages &&
                            this.props.error === null) ? 'none' : 'block'
                    }}>Only the first 1,000 results will be shown
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        totalPages: state.totalPages,
        maximumPages: state.maximumPages,
        error: state.error
    };
};

export default connect(mapStateToProps)(UserInput);

