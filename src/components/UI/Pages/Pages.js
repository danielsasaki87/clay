import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactPaginate from 'react-paginate';
import axios from 'axios';

import classes from './Pages.css';
import * as actionTypes from "../../../store/actions";
import * as utils from "../../utils/utils";

class Pages extends Component {

    pageClickHandler = data => {

        let startCt = new Date().getTime();
        let searchTime;

        // eslint-disable-next-line radix
        axios.get(`https://api.github.com/search/repositories?q=${this.props.typedQuery}&page=${parseInt(data.selected + 1)}&per_page=${this.props.resultsPerPage}`)
            .then(response => {

                let maxPages = Math.ceil(response.data.total_count / this.props.resultsPerPage);
                // if (maxPages > this.props.maximumPages) maxPages = this.props.maximumPages;
                this.props.onSearch(response.data.items, maxPages);
                this.props.onShowResults(true);
                this.props.onErrorFound(null);
                this.props.onShowMessage(response.data.items.length === 0);

                searchTime = utils.calculateSearchTime(startCt);
                this.props.onSearchTime(searchTime);
            })
            .catch((error) => {
                this.props.onSearch([]);
                this.props.onShowResults(false);
                this.props.onErrorFound(error.response.data);
                this.props.onShowMessage(true);
                this.props.onShowSearchTime(false);
            });
    };

    render() {
        return <div className={classes.Pages} style={{
            display: this.props.showResults && this.props.totPages > 0 ? 'flex' : 'none'
        }}>
            <ReactPaginate
                previousLabel={'previous'}
                nextLabel={'next'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={this.props.totPages} // req
                marginPagesDisplayed={2} // req
                pageRangeDisplayed={5} // req
                onPageChange={this.pageClickHandler}
                containerClassName={'pagination'}
                subContainerClassName={'pages pagination'}
                activeClassName={'active'}
            />
        </div>;
    }
}

const mapStateToProps = state => {
    return {
        totPages: state.totalPages,
        maxPages: state.maximumPages,
        showResults: state.showResults,
        typedQuery: state.typedQuery,
        resultsPerPage: state.resultsPerPage
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowResults: (value) => dispatch({type: actionTypes.SHOW_RESULTS, value}),
        onShowMessage: (value) => dispatch({type: actionTypes.SHOW_MESSAGE, value}),
        onErrorFound: (value) => dispatch({type: actionTypes.ERRORS_FOUND, value}),
        onSearch: (value, totalPages) => dispatch({type: actionTypes.SEARCHED, payload: {value: value, totalPages}}),
        onSearchTime: (value) => dispatch({type: actionTypes.SEARCH_TIME, value}),
        onShowSearchTime: (value) => dispatch({type: actionTypes.SHOW_SEARCH_TIME, value}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Pages);