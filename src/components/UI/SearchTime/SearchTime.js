import React, {Component} from 'react';
import {connect} from "react-redux";

import classes from './SearchTime.css';

class SearchTime extends Component {
    render() {
        return (
            <div
                className={classes.SearchTime}
                style={{
                    display: this.props.showSearchTime ? 'flex' : 'none'
                }}>
                Search time: {this.props.searchTime}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        searchTime: state.searchTime,
        showSearchTime: state.showSearchTime
    };
};

export default connect(mapStateToProps)(SearchTime);