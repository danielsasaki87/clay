import React, {Component} from 'react';
import LazyLoad from 'react-lazy-load';
import {connect} from 'react-redux';
import ReactMarkdown from 'react-markdown';

import classes from './QueryDetails.css';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import * as actionTypes from '../../../store/actions';

import axios from "axios";

class QueryDetails extends Component {

    closeDetailsHandler() {
        // console.log('[QueryDetails.js] closeDetailsHandler');
        window.location.hash = '';

        this.props.onShowResults(true);
        this.props.onShowDetails(false);
        this.props.onGetReadme({});
    }

    copyDeepLinkHandler() {
        const el = document.createElement('textarea');
        el.value = window.location;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert("Link copied!");
    }

    searchReadmeHandler() {
        if (Object.entries(this.props.queryDetails).length === 0 && this.props.queryDetails.constructor === Object)
            return;

        // console.log('[QueryDetailsCP.js] searchReadmeHandler');

        let queryDetails = this.props.queryDetails;
        let owner = queryDetails.owner ? queryDetails.owner.login : null;

        axios.get(`https://api.github.com/repos/${owner}/${queryDetails.name}/readme`)
            .then((response) => {
                this.getMarkdown(response.data.download_url);
                this.props.onErrorFound(null);

            })
            .catch((error) => {
                this.props.onShowResults(false);
                this.props.onErrorFound(error.response.data);
                this.props.onShowMessage(true);
                this.props.onShowSearchTime(false);
            });
    }

    getMarkdown(markdownLink) {
        // console.log('[QueryDetailsCP.js] getMarkdown');

        axios.get(markdownLink)
            .then((response) => {
                this.props.onGetReadme(response.data);
                this.props.onErrorFound(null);
            })
            .catch((error) => {
                this.props.onShowResults(false);
                this.props.onErrorFound(error.response.data);
                this.props.onShowMessage(true);
                this.props.onShowSearchTime(false);
            });
    }

    render() {
        // console.log('[QueryDetails.js] render', this.props.showDetails);

        this.searchReadmeHandler();

        let queryDet = this.props.queryDetails;
        let owner = queryDet.owner;

        return (
            !this.props.showDetails ? null :
                <Aux>
                    <div className={classes.QueryDetails}>

                        <button className={classes.btnBack} onClick={() => this.closeDetailsHandler()}>Back</button>

                        <div className={classes.flexContainer}>
                            <div className={classes.name}>{queryDet.name}</div>
                            <div>Created at: {queryDet.created_at}</div>
                            <div>Updated at: {queryDet.updated_at}</div>
                            <div>Forks: {queryDet.forks}</div>

                            <div onClick={() => this.copyDeepLinkHandler()} className={classes.deepLink}>
                                Click to copy the link to this page
                            </div>
                        </div>

                        <div className={classes.flexContainerAvatar}>
                            <LazyLoad>
                                <img className={classes.avatar} src={owner ? owner.avatar_url : null} alt='/'/>
                            </LazyLoad>
                            <div className={classes.author}>Author: {owner ? owner.login : null}</div>
                        </div>

                        <div className={classes.markdown}>
                            <LazyLoad>
                                <ReactMarkdown source={this.props.readmeFile.toString()}/>
                            </LazyLoad>
                        </div>

                    </div>
                </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        showDetails: state.showDetails,
        queryDetails: state.queryDetails,
        readmeFile: state.readmeFile
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowResults: (value) => dispatch({type: actionTypes.SHOW_RESULTS, value}),
        onShowDetails: (value) => dispatch({type: actionTypes.SHOW_DETAILS, value}),
        onErrorFound: (value) => dispatch({type: actionTypes.ERRORS_FOUND, value}),
        onShowSearchTime: (value) => dispatch({type: actionTypes.SHOW_SEARCH_TIME, value}),
        onShowMessage: (value) => dispatch({type: actionTypes.SHOW_MESSAGE, value}),
        onGetReadme: (value) => dispatch({type: actionTypes.LOAD_README, value}),
        onSearchTime: (value) => dispatch({type: actionTypes.SEARCH_TIME, value}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryDetails);