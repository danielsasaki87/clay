import React, {Component} from 'react';
import {connect} from 'react-redux';

import classes from './QueryMessage.css';
import * as actionTypes from "../../../store/actions";

class QueryMessage extends Component {

    hideMessage() {
        this.props.onShowMessage(false);
    }

    render() {
        // console.log('[QueryMessage.js] render', this.props);

        let message = '';
        let messageClass;

        if (this.props.error != null) {
            let error = this.props.error;
            message = `Error! ${error.message}`;
            messageClass = classes.danger;
        } else if (this.props.searchResults.length <= 0) {
            message = 'No repositories found...';
            messageClass = classes.alert;
        }

        return <div className={[classes.QueryMessage, messageClass].join(' ')}
                    onClick={() => this.hideMessage()}
                    style={{
                        opacity: this.props.showMessage > 0 ? '1' : '0'
                    }}>
            {message}
        </div>;
    }

}

const mapStateToProps = state => {
    return {
        showMessage: state.showMessage,
        error: state.error,
        searchResults: state.searchResults
    };
};


const mapDispatchToProps = dispatch => {
    return {
        onShowMessage: (value) => dispatch({type: actionTypes.SHOW_MESSAGE, value}),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(QueryMessage);