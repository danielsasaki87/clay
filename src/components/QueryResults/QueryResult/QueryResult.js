import React from 'react';
import LazyLoad from 'react-lazy-load';

import classes from './QueryResult.css';

const queryResult = (props) => (
    <div className={classes.QueryResult} onClick={props.clicked}>
        <LazyLoad height={50} offsetVertical={25}>
            <img className={classes.avatar} src={props.avatar} alt='/'/>
        </LazyLoad>

        <div className={classes.name}>{props.name}</div>
        <div className={classes.author}>
            {props.author}</div>
    </div>
);

export default queryResult;