import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';

import classes from './QueryResults.css';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import QueryResult from './QueryResult/QueryResult';
import Pages from '../../components/UI/Pages/Pages';
import * as actionTypes from "../../store/actions";
import * as utils from "../utils/utils";

class QueryResults extends Component {

    state = {
        currentPage: 1
    };

    clickResultHandler = (id) => {
        this.props.onShowResults(false);
        window.location.hash = 'id=' + id;

        this.props.onShowSearchTime(false);

        let startCt = new Date().getTime();
        let searchTime;

        axios.get('https://api.github.com/repositories/' + id)
            .then((response) => {
                // console.log(response);
                // this.props.onShowDetails(true);
                this.props.onQueryDetails(response.data, true);

                searchTime = utils.calculateSearchTime(startCt);
                this.props.onSearchTime(searchTime);
            })
            .catch((error) => {
                this.props.onSearch([]);
                this.props.onShowResults(false);
                this.props.onErrorFound(error.response.data);
                this.props.onShowMessage(true);
                this.props.onShowSearchTime(false);
            });
    };

    render() {
        // console.log('[QueryResults.js] render', this.props.showResults);

        let results = this.props.searchResults.map(res => {
            return <QueryResult
                key={res.id}
                avatar={res.owner.avatar_url}
                name={res.name}
                author={res.owner.login}
                clicked={() => this.clickResultHandler(res.id)}/>
        });

        return (
            !this.props.showResults ? null :
                <Aux>
                    <Pages/>

                    {/*<div>Showing results from {this.props.resultsPerPage} to {}</div>*/}

                    <div className={classes.QueryResults}>
                        {results}
                    </div>
                </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        showResults: state.showResults,
        searchResults: state.searchResults,
        resultsPerPage: state.resultsPerPage,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onShowDetails: (value) => dispatch({type: actionTypes.SHOW_DETAILS, value}),
        onShowResults: (value) => dispatch({type: actionTypes.SHOW_RESULTS, value}),
        onQueryDetails: (value, show) => dispatch({
            type: actionTypes.QUERY_DETAILS,
            payload: {value, show}
        }),
        onSearchTime: (value) => dispatch({type: actionTypes.SEARCH_TIME, value}),
        onShowSearchTime: (value) => dispatch({type: actionTypes.SHOW_SEARCH_TIME, value}),
        onErrorFound: (value) => dispatch({type: actionTypes.ERRORS_FOUND, value}),
        onShowMessage: (value) => dispatch({type: actionTypes.SHOW_MESSAGE, value}),
        onSearch: (value, totalPages) => dispatch({
            type: actionTypes.SEARCHED,
            payload: {value, totalPages}
        }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryResults);
