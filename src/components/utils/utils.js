export const calculateSearchTime = (startTime) => {
    return (new Date().getTime() - startTime) / 1000;
};
