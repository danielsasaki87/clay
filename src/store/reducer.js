import * as actionTypes from './actions';

const initialState = {
    searchResults: [],
    showResults: false,
    error: null,
    typedQuery: '',
    showMessage: false,
    totalPages: 0,
    maximumPages: 20,
    resultsPerPage: 50,
    showDetails: false,
    queryDetails: {},
    readmeFile: {},
    searchTime: -1,
    showSearchTime: false
};

const reducer = (state = initialState, action) => {
    // console.log(action.type, action.value);

    switch (action.type) {
        case actionTypes.SHOW_MESSAGE:
            return {
                ...state,
                showMessage: action.value
            };
        case actionTypes.SHOW_RESULTS:
            return {
                ...state,
                showResults: action.value
            };
        case actionTypes.ERRORS_FOUND:
            return {
                ...state,
                error: action.value
            };
        case actionTypes.SEARCHED:
            return {
                ...state,
                searchResults: action.payload.value,
                totalPages: action.payload.totalPages < state.maximumPages ?
                    action.payload.totalPages :
                    state.maximumPages,
            };
        case actionTypes.TYPING:
            return {
                ...state,
                typedQuery: action.value
            };
        case actionTypes.SHOW_DETAILS:
            return {
                ...state,
                showDetails: action.value
            };
        case actionTypes.QUERY_DETAILS:
            return {
                ...state,
                queryDetails: action.payload.value,
                showDetails: action.payload.show
            };
        case actionTypes.SEARCH_TIME:
            return {
                ...state,
                searchTime: action.value,
                showSearchTime: true
            };
        case actionTypes.SHOW_SEARCH_TIME:
            return {
                ...state,
                showSearchTime: action.value
            };
        case actionTypes.LOAD_README:
            return {
                ...state,
                readmeFile: action.value
            };

        default:
            return state;
    }
};

export default reducer;