export const SHOW_RESULTS = 'SHOW_RESULTS';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const SHOW_DETAILS = 'SHOW_DETAILS';

export const ERRORS_FOUND = 'ERRORS_FOUND';
export const TYPING = 'TYPING';
export const QUERY_DETAILS = 'QUERY_DETAILS';

export const SEARCHED = 'SEARCHED';
export const SEARCH_TIME = 'SEARCH_TIME';
export const SHOW_SEARCH_TIME = 'SHOW_SEARCH_TIME';
export const LOAD_README = 'LOAD_README';
